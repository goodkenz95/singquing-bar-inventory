<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicalCountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physical_counts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('product_id')->nullable();
            $table->string('product_name',150)->nullable();
            $table->integer('supplier_id')->nullable();
            $table->string('supplier_name',25)->nullable();
            $table->integer('qty')->default(0);
            $table->timestamps();
            $table->softDeletes();
            // $table->decimal('cost',25,2)->default("0.00")->nullable();
            // $table->decimal('total_cost',25,2)->default("0.00")->nullable();
            // $table->decimal('discount',5,2)->default("0.00")->nullable();
            // $table->decimal('final_cost',25,2)->default("0.00")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physical_counts');
    }
}
