<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockInTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_in_details', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('stock_in_header_id')->default('0');
            $table->integer('product_id')->nullable();
            $table->string('product_name',150)->nullable();
            $table->integer('supplier_id')->nullable();
            $table->string('supplier_name',25)->nullable();
            $table->integer('qty')->default(0);
            // $table->enum('status',['draft','posted','cancelled'])->default('draft');
            $table->timestamps();
            $table->softDeletes();
            // $table->decimal('cost',25,2)->default("0.00")->nullable();
            // $table->decimal('total_cost',25,2)->default("0.00")->nullable();
            // $table->decimal('discount',5,2)->default("0.00")->nullable();
            // $table->decimal('final_cost',25,2)->default("0.00")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_in_details');
    }
}
