<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockOutHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_out_headers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->default('0');
            $table->integer('admin_user_id')->default('0');
            $table->string('code','15')->nullable();
            $table->integer('total_qty')->default(0);
            $table->enum('status',['draft','posted','cancelled'])->default('draft');
            $table->timestamps();
            $table->softDeletes();
            // $table->decimal('cost',25,2)->default("0.00")->nullable();
            // $table->decimal('total_cost',25,2)->default("0.00")->nullable();
            // $table->decimal('discount',5,2)->default("0.00")->nullable();
            // $table->decimal('final_cost',25,2)->default("0.00")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_out_headers');
    }
}
