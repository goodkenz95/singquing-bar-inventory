<?php

use App\Laravel\Models\User;
use Illuminate\Database\Seeder;

class AdminAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(['name' => "Super User", 'email' => "admin@domain.com", 'password' => bcrypt('admin'),'username' => "admin" ,'type' => "super_user",'position' => "Administrator"]);
    }
}
