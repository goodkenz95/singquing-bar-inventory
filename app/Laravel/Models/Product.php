<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'supplier_id','name','price','status'
    ];

    public function supplier(){
    	return $this->hasOne('App\Laravel\Models\Supplier','id','supplier_id');
    }

}
