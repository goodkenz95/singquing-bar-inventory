<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'token',
    ];

    public $timestamps = false;
}
