<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockInHeader extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	
    ];

    public function encoder(){
    	return $this->hasOne('App\Laravel\Models\User','id','user_id');
    }

    public function admin(){
    	return $this->hasOne('App\Laravel\Models\User','id','admin_user_id');
    }
}
