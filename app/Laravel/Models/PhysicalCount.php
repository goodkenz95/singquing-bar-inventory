<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PhysicalCount extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'supplier_id','supplier_name','product_id','product_name'
    ];


    public function product(){
    	return $this->hasOne('App\Laravel\Models\Product','id','product_id');
    }
}
