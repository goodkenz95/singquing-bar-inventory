<?php

namespace App\Laravel\Requests\Backoffice;

use App\Laravel\Requests\RequestManager;

class StockOutRequest extends RequestManager
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ? : 0;

        $rules = [
            'product_id'          => "required",
            'qty'      => "required|numeric|min:1",
        ];

        return $rules;
    }


    public function messages() {
        return [
            'min'       => "Min. qty is 1",
            'numeric'   => "Field must be in number.",
            'required'  => "Field is required.",
            'email.required_if' => "Email required when username not specify.",
            'password.confirmed' => "Password not match." 
        ];
    }
}
