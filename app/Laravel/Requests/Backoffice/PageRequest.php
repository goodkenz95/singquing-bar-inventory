<?php

namespace App\Laravel\Requests\Backoffice;

use App\Laravel\Requests\RequestManager;

class PageRequest extends RequestManager
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ? : 0;

        $rules = [
            'title' => "required",
            'file' => "image",
        ];

        if($this->has('slug')) {
            $rules['slug'] = "unique:pages,slug," . $id;
        }

        return $rules;
    }
}
