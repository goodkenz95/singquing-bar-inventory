<?php

namespace App\Laravel\Requests\Backoffice;

use App\Laravel\Requests\RequestManager;

class UserRequest extends RequestManager
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ? : 0;

        $rules = [
            'name'          => "required",
            'username'      => "required|unique:users,username,".$id,
            'email'         => "required_if:username,false|email|unique:users,email,".$id,
            'password'      => "required|confirmed",
            'position'      => "required",
            // 'type'          => "required"
        ];

        return $rules;
    }


    public function messages() {
        return [
            'required'  => "Field is required.",
            'email.required_if' => "Email required when username not specify.",
            'password.confirmed' => "Password not match." 
        ];
    }
}
