<?php

namespace App\Laravel\Requests\Backoffice;

use App\Laravel\Requests\RequestManager;
use Illuminate\Support\Facades\Auth;

class ProfileRequest extends RequestManager
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        $rules = [
            'name' => "required",
            'email' => "required|email|unique:users,email," . $user->id,
            'password' => "required|old_password:" . $user->id,
        ];

        return $rules;
    }

    public function messages() {
        return [
            'password.old_password' => "Incorrect password.",
        ];
    }
}
