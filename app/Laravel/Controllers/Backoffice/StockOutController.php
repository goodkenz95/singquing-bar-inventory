<?php

namespace App\Laravel\Controllers\Backoffice;

use App\Laravel\Requests\Backoffice\ProductRequest;
use App\Laravel\Requests\Backoffice\StockOutRequest;

use App\Laravel\Models\Supplier;
use App\Laravel\Models\Product;
use App\Laravel\Models\PhysicalCount;
use App\Laravel\Models\StockOutHeader;
use App\Laravel\Models\StockOutDetail;


use ImageUploader,Auth,Helper,Carbon,DB,PDF;

class StockOutController extends Controller
{

	protected $data = array();

    public function __construct(){
        $this->data['statuses'] = ['active' => "Active",'inactive' => "Inactive"];
        $this->data['suppliers'] = ['' => "Choose supplier"] + Supplier::where('status','active')->pluck('name','id')->toArray();
    }

    public function index() {
        $this->data['stock_outs'] = StockOutHeader::orderBy('status','draft')->get();
    	return view('backoffice.stock-out.index', $this->data);
    }

    public function create() {
        $this->data['auth'] = Auth::user();

        $current_year = Helper::date_format(Carbon::now(),"Y");

        $current_record = StockOutHeader::whereRaw("YEAR(created_at) = '{$current_year}'")->withTrashed()->count();
        $new_stockout = new StockOutHeader;
        $new_stockout->code = "OUT-{$current_year}-".str_pad(++$current_record, 5,"0",STR_PAD_LEFT);
        $new_stockout->user_id = $this->data['auth']->id;
        $new_stockout->status = "draft";

        if(!$new_stockout->save()){
            session()->flash('notification-status','failed');
            session()->flash('notification-msg','Server error while creating record. Please try again.');
            return redirect()->route('backoffice.stock_out.index');
        }

        return redirect()->route('backoffice.stock_out.edit',[$new_stockout->id]);
    	// return view('backoffice.stock-out.create', $this->data);
    }

    public function edit($id = 0) {
        $header = StockOutHeader::find($id);
        if($header) {
            // $suppliers = Supplier::where('status','active')->get();
            $suppliers = PhysicalCount::distinct('supplier_id')->get();
            $this->data['products'] = ['' => "Choose Product"];
            
            foreach($suppliers as $index => $supplier){
                $products = PhysicalCount::select(DB::raw("CONCAT(product_name,' (Qty. ',qty,')') as name"),'product_id')->where('supplier_id',$supplier->supplier_id)
                                ->pluck('name','product_id');
                if($products->count() > 0){
                    $this->data['products']["{$supplier->supplier_name}"] = $products->toArray();
                }
            }

            $this->data['header'] = $header;
            $this->data['details'] = StockOutDetail::where('stock_out_header_id',$id)->get();
            return view('backoffice.stock-out.edit', $this->data);
        } else {
            $this->_record_not_found();
        }
    }

    public function add_product(StockOutRequest $request,$id = 0 ){
        $header = StockOutHeader::find($id);
        if($header) {
            // $product = Product::find($request->get('product_id'));
            $product = PhysicalCount::where('product_id',$request->get('product_id'))->first();


            if($product AND $product->qty < $request->get('qty')){
                session()->flash('notification-status','failed');
                session()->flash('notification-msg','Insufficient Stock.');
                return redirect()->back(); 
            }

            $detail = StockOutDetail::where('product_id',$request->get('product_id'))
                                ->where('stock_out_header_id',$id)
                                ->first();
            session()->flash('notification-status','success');
            session()->flash('notification-msg','Record updated successfully.');

            if(!$detail){
                $detail = new StockOutDetail;
                $detail->stock_out_header_id = $id;
                $detail->product_id = $product->product_id;
                $detail->product_name = $product->product_name;
                $detail->supplier_id = $product->supplier_id;
                $detail->supplier_name = $product->supplier_name;

                session()->flash('notification-msg','New record has been added.');
            }

            $detail->qty = $request->get('qty');

            if(!$detail->save()){
                session()->flash('notification-status','failed');
                session()->flash('notification-msg','Server error while storing record. Please try again.');
                return redirect()->back();
            }

            $stocks = StockOutDetail::where('stock_out_header_id',$id)->get();
            $sum_qty = 0;
            foreach($stocks as $index => $stock){
                $sum_qty += $stock->qty; 
            }

            $header->total_qty = $sum_qty;

            $header->save();

            callback:
            return redirect()->route('backoffice.stock_out.edit',[$header->id]);

        } else {
            $this->_record_not_found();
        }
    }


    public function remove_product($id = 0 , $detail_id = 0){
        $detail = StockOutDetail::where('id',$detail_id)->where('stock_out_header_id',$id)->first();
        if($detail) {
            $header = StockOutHeader::find($id);
            session()->flash('notification-status','success');
            session()->flash('notification-msg','Product stock successfully deleted.');
           if(!$detail->delete()){
               session()->flash('notification-status','failed');
               session()->flash('notification-msg','Server error while deleting record. Please try again.');
               return redirect()->back();
           } 

           $stocks = StockOutDetail::where('stock_out_header_id',$id)->get();
           $sum_qty = 0;
           foreach($stocks as $index => $stock){
               $sum_qty += $stock->qty; 
           }

           $header->total_qty = $sum_qty;

           $header->save();
           return redirect()->route('backoffice.stock_out.edit',[$header->id]);

        } else {
            $this->_record_not_found();
        }
    }

    public function approve($id = 0){
        $header = StockOutHeader::find($id);

        if($header){
            $header->status = "posted";
            $header->admin_user_id = Auth::user()->id;
            $header->save();

            $details = StockOutDetail::where('stock_out_header_id',$id)->get();

            foreach($details as $index => $detail){
                $inventory = PhysicalCount::where('product_id',$detail->product_id)->first();

                // if(!$inventory){
                //     $inventory = new PhysicalCount;
                //     $inventory->product_id = $detail->product_id;
                //     $inventory->product_name = $detail->product_name;
                //     $inventory->supplier_id = $detail->supplier_id;
                //     $inventory->supplier_name = $detail->supplier_name;
                //     $inventory->qty = 0;
                // }

                $inventory->qty -= $detail->qty;

                $inventory->save();
            }

            session()->flash('notification-status','success');
            session()->flash('notification-msg',"Stock out : {$header->code} successfully removed stock to physical inventory");
            return redirect()->route('backoffice.stock_out.index');
        } else {
            $this->_record_not_found();
        }
    }

    public function destroy($id = 0) {
    	$header = StockOutHeader::find($id);
        if($header) {
            $header->status = "cancelled";
            $header->admin_user_id = Auth::user()->id;
            $header->save();
            // $header->delete();
            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'Transaction has been cancelled.');
            return redirect()->route('backoffice.stock_out.index');
        } else {
            $this->_record_not_found();
        }
    }

    public function pdf($id = 0){
        $header = StockOutHeader::find($id);
        if($header) {
            $suppliers = Supplier::where('status','active')->get();
            $this->data['products'] = ['' => "Choose Product"];
            
            foreach($suppliers as $index => $supplier){
                $products = Product::where('supplier_id',$supplier->id)
                                ->pluck('name','id');
                if($products->count() > 0){
                    $this->data['products']["{$supplier->name}"] = $products->toArray();
                }
            }

            $this->data['header'] = $header;
            $this->data['details'] = StockOutDetail::where('stock_out_header_id',$id)->get();
            // return view('backoffice.stock-in.edit', $this->data);
            $pdf = PDF::loadView('pdf.stock-out', $this->data);
            return $pdf->stream("{$header->code}.pdf");
        } else {
            $this->_record_not_found();
        }
    }

    public function trash() {
        $this->data['users'] = Product::onlyTrashed()->orderBy('deleted_at', "desc")->get();
        return view('backoffice.stock-out.trash', $this->data);
    }

    public function restore($id = 0) {
        $user = Product::onlyTrashed()->find($id);
        if($user) {
            $user->restore();
            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'A record has been restored.');
            return redirect()->route('backoffice.stock_out.index');
        } else {
            $this->_record_not_found();
        }
    }

    private function _record_not_found() {
        session()->flash('notification-status', 'error');
        session()->flash('notification-msg', 'Record not found.');
        return redirect()->route('backoffice.stock_out.index');
    }
}
