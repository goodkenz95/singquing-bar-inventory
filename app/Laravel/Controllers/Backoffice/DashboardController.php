<?php

namespace App\Laravel\Controllers\Backoffice;

use App\Laravel\Models\PhysicalCount;
use App\Laravel\Models\StockInHeader;
use App\Laravel\Models\StockOutHeader;


use Carbon,Helper,DB;

class DashboardController extends Controller
{

	protected $data = array();

    public function index() {
    	$this->data['date_today'] = Helper::date_db(Carbon::now());
    	$this->data['total_stocks'] = PhysicalCount::sum('qty');
    	$this->data['stockin_today'] = StockInHeader::whereRaw("DATE(updated_at) = '{$this->data['date_today']}'")->where('status','posted')->sum('total_qty');
    	$this->data['stockout_today'] = StockOutHeader::whereRaw("DATE(updated_at) = '{$this->data['date_today']}'")->where('status','posted')->sum('total_qty');
    	$this->data['thisweek_stockin'] = StockInHeader::whereBetween(DB::raw("DATE(updated_at)"),
    			[Helper::date_db(Carbon::now()->startOfWeek()),Helper::date_db(Carbon::now()->endOfWeek())])->orderBy('updated_at','DESC')->get();

    	// $this->data['thisweek_stockin_posted'] = StockInHeader::whereBetween(DB::raw("DATE(updated_at)"),
    	// 		[Helper::date_db(Carbon::now()->startOfWeek()),Helper::date_db(Carbon::now()->endOfWeek())])->where('status',"posted")->orderBy('updated_at','DESC')->sum('total_qty');

    	// $this->data['thisweek_stockin_draft'] = StockInHeader::whereBetween(DB::raw("DATE(updated_at)"),
    	// 		[Helper::date_db(Carbon::now()->startOfWeek()),Helper::date_db(Carbon::now()->endOfWeek())])->where('status',"draft")->orderBy('updated_at','DESC')->sum('total_qty');

    	$this->data['thisweek_stockout'] = StockOutHeader::whereBetween(DB::raw("DATE(updated_at)"),
    			[Helper::date_db(Carbon::now()->startOfWeek()),Helper::date_db(Carbon::now()->endOfWeek())])->orderBy('updated_at','DESC')->get();

    	// $this->data['thisweek_stockout_posted'] = StockOutHeader::whereBetween(DB::raw("DATE(updated_at)"),
    	// 		[Helper::date_db(Carbon::now()->startOfWeek()),Helper::date_db(Carbon::now()->endOfWeek())])->where('status',"posted")->orderBy('updated_at','DESC')->sum('total_qty');

    	// $this->data['thisweek_stockout_draft'] = StockOutHeader::whereBetween(DB::raw("DATE(updated_at)"),
    	// 		[Helper::date_db(Carbon::now()->startOfWeek()),Helper::date_db(Carbon::now()->endOfWeek())])->where('status',"draft")->orderBy('updated_at','DESC')->sum('total_qty');

    	return view('backoffice.dashboard', $this->data);
    }
}
