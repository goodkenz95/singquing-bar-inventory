<?php

namespace App\Laravel\Controllers\Backoffice;

use App\Laravel\Requests\Backoffice\ProductRequest;
use App\Laravel\Requests\Backoffice\StockInRequest;

use App\Laravel\Models\Supplier;
use App\Laravel\Models\Product;
use App\Laravel\Models\PhysicalCount;
use App\Laravel\Models\StockInHeader;
use App\Laravel\Models\StockInDetail;

use ImageUploader,Auth,Helper,Carbon,PDF;

class StockInController extends Controller
{

	protected $data = array();

    public function __construct(){
        $this->data['statuses'] = ['active' => "Active",'inactive' => "Inactive"];
        $this->data['suppliers'] = ['' => "Choose supplier"] + Supplier::where('status','active')->pluck('name','id')->toArray();
    }

    public function index() {
        $this->data['stock_ins'] = StockInHeader::orderBy('status','draft')->get();
    	return view('backoffice.stock-in.index', $this->data);
    }

    public function create() {
        $this->data['auth'] = Auth::user();

        $current_year = Helper::date_format(Carbon::now(),"Y");

        $current_record = StockInHeader::whereRaw("YEAR(created_at) = '{$current_year}'")->withTrashed()->count();
        $new_stockin = new StockInHeader;
        $new_stockin->code = "IN-{$current_year}-".str_pad(++$current_record, 5,"0",STR_PAD_LEFT);
        $new_stockin->user_id = $this->data['auth']->id;
        $new_stockin->status = "draft";

        if(!$new_stockin->save()){
            session()->flash('notification-status','failed');
            session()->flash('notification-msg','Server error while creating record. Please try again.');
            return redirect()->route('backoffice.stock_in.index');
        }

        return redirect()->route('backoffice.stock_in.edit',[$new_stockin->id]);
    	// return view('backoffice.stock-in.create', $this->data);
    }

    public function edit($id = 0) {
        $header = StockInHeader::find($id);
        if($header) {
            $suppliers = Supplier::where('status','active')->get();
            $this->data['products'] = ['' => "Choose Product"];
            
            foreach($suppliers as $index => $supplier){
                $products = Product::where('supplier_id',$supplier->id)
                                ->pluck('name','id');
                if($products->count() > 0){
                    $this->data['products']["{$supplier->name}"] = $products->toArray();
                }
            }

            $this->data['header'] = $header;
            $this->data['details'] = StockInDetail::where('stock_in_header_id',$id)->get();
            return view('backoffice.stock-in.edit', $this->data);
        } else {
            $this->_record_not_found();
        }
    }

    public function add_product(StockInRequest $request,$id = 0 ){
        $header = StockInHeader::find($id);
        if($header) {
            $product = Product::find($request->get('product_id'));
            $detail = StockInDetail::where('product_id',$request->get('product_id'))
                                ->where('stock_in_header_id',$id)
                                ->first();
            session()->flash('notification-status','success');
            session()->flash('notification-msg','Record updated successfully.');

            if(!$detail){
                $detail = new StockInDetail;
                $detail->stock_in_header_id = $id;
                $detail->product_id = $product->id;
                $detail->product_name = $product->name;
                $detail->supplier_id = $product->supplier_id;
                $detail->supplier_name = $product->supplier?$product->supplier->name:"-";

                session()->flash('notification-msg','New record has been added.');
            }

            $detail->qty = $request->get('qty');

            if(!$detail->save()){
                session()->flash('notification-status','failed');
                session()->flash('notification-msg','Server error while storing record. Please try again.');
                return redirect()->back();
            }

            $stocks = StockInDetail::where('stock_in_header_id',$id)->get();
            $sum_qty = 0;
            foreach($stocks as $index => $stock){
                $sum_qty += $stock->qty; 
            }

            $header->total_qty = $sum_qty;

            $header->save();
            return redirect()->route('backoffice.stock_in.edit',[$header->id]);

        } else {
            $this->_record_not_found();
        }
    }


    public function remove_product($id = 0 , $detail_id = 0){
        $detail = StockInDetail::where('id',$detail_id)->where('stock_in_header_id',$id)->first();
        if($detail) {
            $header = StockInHeader::find($id);
            session()->flash('notification-status','success');
            session()->flash('notification-msg','Product stock successfully deleted.');
           if(!$detail->delete()){
               session()->flash('notification-status','failed');
               session()->flash('notification-msg','Server error while deleting record. Please try again.');
               return redirect()->back();
           } 

           $stocks = StockInDetail::where('stock_in_header_id',$id)->get();
           $sum_qty = 0;
           foreach($stocks as $index => $stock){
               $sum_qty += $stock->qty; 
           }

           $header->total_qty = $sum_qty;

           $header->save();
           return redirect()->route('backoffice.stock_in.edit',[$header->id]);

        } else {
            $this->_record_not_found();
        }
    }

    public function approve($id = 0){
        $header = StockInHeader::find($id);

        if($header){
            $header->status = "posted";
            $header->admin_user_id = Auth::user()->id;
            $header->save();

            $details = StockInDetail::where('stock_in_header_id',$id)->get();

            foreach($details as $index => $detail){
                $inventory = PhysicalCount::where('product_id',$detail->product_id)->first();

                if(!$inventory){
                    $inventory = new PhysicalCount;
                    $inventory->product_id = $detail->product_id;
                    $inventory->product_name = $detail->product_name;
                    $inventory->supplier_id = $detail->supplier_id;
                    $inventory->supplier_name = $detail->supplier_name;
                    $inventory->qty = 0;
                }

                $inventory->qty += $detail->qty;

                $inventory->save();
            }

            session()->flash('notification-status','success');
            session()->flash('notification-msg',"Stock in : {$header->code} successfully added to physical inventory");
            return redirect()->route('backoffice.inventory.index');
        } else {
            $this->_record_not_found();
        }
    }

    public function destroy($id = 0) {
    	$header = StockInHeader::find($id);
        if($header) {
            $header->status = "cancelled";
            $header->admin_user_id = Auth::user()->id;
            $header->save();
            // $header->delete();
            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'Transaction has been cancelled.');
            return redirect()->route('backoffice.stock_in.index');
        } else {
            $this->_record_not_found();
        }
    }

    public function trash() {
        $this->data['users'] = Product::onlyTrashed()->orderBy('deleted_at', "desc")->get();
        return view('backoffice.stock-in.trash', $this->data);
    }

    public function restore($id = 0) {
        $user = Product::onlyTrashed()->find($id);
        if($user) {
            $user->restore();
            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'A record has been restored.');
            return redirect()->route('backoffice.stock_in.index');
        } else {
            $this->_record_not_found();
        }
    }

    public function pdf($id = 0){
        

        $header = StockInHeader::find($id);
        if($header) {
            $suppliers = Supplier::where('status','active')->get();
            $this->data['products'] = ['' => "Choose Product"];
            
            foreach($suppliers as $index => $supplier){
                $products = Product::where('supplier_id',$supplier->id)
                                ->pluck('name','id');
                if($products->count() > 0){
                    $this->data['products']["{$supplier->name}"] = $products->toArray();
                }
            }

            $this->data['header'] = $header;
            $this->data['details'] = StockInDetail::where('stock_in_header_id',$id)->get();
            // return view('backoffice.stock-in.edit', $this->data);
            $pdf = PDF::loadView('pdf.stock-in', $this->data);
            return $pdf->stream("{$header->code}.pdf");
        } else {
            $this->_record_not_found();
        }
    }

    private function _record_not_found() {
        session()->flash('notification-status', 'error');
        session()->flash('notification-msg', 'Record not found.');
        return redirect()->route('backoffice.stock_in.index');
    }
}
