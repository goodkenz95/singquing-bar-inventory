<?php

namespace App\Laravel\Controllers\Backoffice;

use App\Laravel\Requests\Backoffice\UserRequest;
use App\Laravel\Models\User;
use ImageUploader;

class UserController extends Controller
{

	protected $data = array();

    public function __construct(){
        $this->data['positions'] = ['' => "Choose Job position",'administrator' => "Administrator" ,'manager' => "Manager",'cashier' => "Cashier"];
        $this->data['types'] = ['' => "Choose User type",'super_user' => "Master Admin",'admin' => "Administrator", 'employee' => "Employee"];
        $this->data['genders'] = ['male' => "Male",'female' => "Female"];
    }

    public function index() {
        $this->data['users'] = User::where('id','<>',1)->orderBy('updated_at', "desc")->get();
    	return view('backoffice.user.index', $this->data);
    }

    public function create() {
    	return view('backoffice.user.create', $this->data);
    }

    public function store(UserRequest $request) {
    	$new_user = new User;
        $new_user->fill($request->except('password'));
        $new_user->password = bcrypt($request->get("password"));
        $new_user->save();

        session()->flash('notification-status', 'success');
        session()->flash('notification-msg', 'New account has been added.');
        return redirect()->route('backoffice.user.index');
    }

    public function edit($id = 0) {
        $user = User::find($id);
        if($user) {
            $this->data['user'] = $user;
            return view('backoffice.user.edit', $this->data);
        } else {
            $this->_record_not_found();
        }
    }

    public function update(UserRequest $request, $id = 0) {
    	$user = User::find($id);
        if($user) {
            $user->fill($request->except('password'));
            $user->password = bcrypt($request->get("password"));

            $user->save();

            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'Account has been updated.');
            return redirect()->route('backoffice.user.index');
        } else {
            $this->_record_not_found();
        }
    }

    public function destroy($id = 0) {
    	$user = User::find($id);
        if($user) {
            $user->username = $user->id;
            $user->email = $user->id."@deleted.com";
            $user->save();
            $user->delete();
            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'A record has been deleted.');
            return redirect()->route('backoffice.user.index');
        } else {
            $this->_record_not_found();
        }
    }

    public function trash() {
        $this->data['users'] = User::onlyTrashed()->orderBy('deleted_at', "desc")->get();
        return view('backoffice.user.trash', $this->data);
    }

    public function restore($id = 0) {
        $user = User::onlyTrashed()->find($id);
        if($user) {
            $user->restore();
            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'A record has been restored.');
            return redirect()->route('backoffice.user.index');
        } else {
            $this->_record_not_found();
        }
    }

    private function _record_not_found() {
        session()->flash('notification-status', 'error');
        session()->flash('notification-msg', 'Record not found.');
        return redirect()->route('backoffice.user.index');
    }
}
