<?php

namespace App\Laravel\Controllers\Backoffice;

use App\Laravel\Requests\Backoffice\ImageSliderRequest;
use App\Laravel\Models\ImageSlider;
use ImageUploader;

class ImageSliderController extends Controller
{

	protected $data = array();

    public function index() {
        $this->data['images'] = ImageSlider::orderBy('sorting',"ASC")->get();
    	return view('backoffice.imageslider.index', $this->data);
    }

    public function create() {
    	return view('backoffice.imageslider.create', $this->data);
    }

    public function store(ImageSliderRequest $request) {
    	$image = new ImageSlider;
        $image->fill($request->all());
        $image->fill(ImageUploader::store('file', 'uploads/imageslider'));
        $image->save();

        session()->flash('notification-status', 'success');
        session()->flash('notification-msg', 'New record has been added.');
        return redirect()->route('backoffice.imageslider.index');
    }

    public function edit($id = 0) {
        $image = ImageSlider::find($id);
        if($image) {
            $this->data['image'] = $image;
            return view('backoffice.imageslider.edit', $this->data);
        } else {
            $this->_record_not_found();
        }
    }

    public function update(ImageSliderRequest $request, $id = 0) {
    	$image = ImageSlider::find($id);
        if($image) {
            $image->fill($request->all());

            $image->fill(ImageUploader::store('file', 'uploads/imageslider'));
            $image->save();

            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'A record has been updated.');
            return redirect()->route('backoffice.imageslider.index');
        } else {
            $this->_record_not_found();
        }
    }

    public function destroy($id = 0) {
    	$image = ImageSlider::find($id);
        if($image) {
            ImageUploader::destroy($image->directory, $image->filename); // Delete the images associated with this page
            $image->update(['path' => null, 'directory' => null, 'filename' => null]); // Then set the image fields back to NULL
            $image->delete();
            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'A record has been deleted.');
            return redirect()->route('backoffice.imageslider.index');
        } else {
            $this->_record_not_found();
        }
    }

    public function trash() {
        $this->data['images'] = ImageSlider::onlyTrashed()->orderBy('deleted_at', "desc")->get();
        return view('backoffice.imageslider.trash', $this->data);
    }

    public function restore($id = 0) {
        $image = ImageSlider::onlyTrashed()->find($id);
        if($image) {
            $image->restore();
            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'A record has been restored.');
            return redirect()->route('backoffice.imageslider.index');
        } else {
            $this->_record_not_found();
        }
    }

    private function _record_not_found() {
        session()->flash('notification-status', 'error');
        session()->flash('notification-msg', 'Record not found.');
        return redirect()->route('backoffice.imageslider.index');
    }
}
