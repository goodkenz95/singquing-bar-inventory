<?php

namespace App\Laravel\Controllers\Backoffice;

use App\Laravel\Requests\Backoffice\PageRequest;
use App\Laravel\Models\Page;
use ImageUploader;

class PageController extends Controller
{

	protected $data = array();

    public function index() {
        $this->data['pages'] = Page::orderBy('updated_at', "desc")->get();
    	return view('backoffice.page.index', $this->data);
    }

    public function create() {
    	return view('backoffice.page.create', $this->data);
    }

    public function store(PageRequest $request) {
    	$page = new Page;
        $page->fill($request->all());

        if(!$request->get('slug', FALSE)) {
            $slug = str_slug($request->get('title'));
            $count = Page::where('slug', $slug)->count();
            $page->slug = $slug . ($count ? ++$count : NULL);
        }

        $page->fill(ImageUploader::store('file', 'uploads/pages'));
        $page->save();

        session()->flash('notification-status', 'success');
        session()->flash('notification-msg', 'New record has been added.');
        return redirect()->route('backoffice.page.index');
    }

    public function edit($id = 0) {
        $page = Page::find($id);
        if($page) {
            $this->data['page'] = $page;
            return view('backoffice.page.edit', $this->data);
        } else {
            $this->_record_not_found();
        }
    }

    public function update(PageRequest $request, $id = 0) {
    	$page = Page::find($id);
        if($page) {
            $page->fill($request->all());

            if(!$request->get('slug', FALSE)) {
                $slug = str_slug($request->get('title'));
                $count = Page::where('slug', $slug)->count();
                $page->slug = $slug . ($count ? ++$count : NULL);
            }

            $page->fill(ImageUploader::store('file', 'uploads/pages'));
            $page->save();

            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'A record has been updated.');
            return redirect()->route('backoffice.page.index');
        } else {
            $this->_record_not_found();
        }
    }

    public function destroy($id = 0) {
    	$page = Page::find($id);
        if($page) {
            ImageUploader::destroy($page->directory, $page->filename); // Delete the images associated with this page
            $page->update(['path' => null, 'directory' => null, 'filename' => null]); // Then set the image fields back to NULL
            $page->delete();
            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'A record has been deleted.');
            return redirect()->route('backoffice.page.index');
        } else {
            $this->_record_not_found();
        }
    }

    public function trash() {
        $this->data['pages'] = Page::onlyTrashed()->orderBy('deleted_at', "desc")->get();
        return view('backoffice.page.trash', $this->data);
    }

    public function restore($id = 0) {
        $page = Page::onlyTrashed()->find($id);
        if($page) {
            $page->restore();
            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'A record has been restored.');
            return redirect()->route('backoffice.page.index');
        } else {
            $this->_record_not_found();
        }
    }

    private function _record_not_found() {
        session()->flash('notification-status', 'error');
        session()->flash('notification-msg', 'Record not found.');
        return redirect()->route('backoffice.page.index');
    }
}
