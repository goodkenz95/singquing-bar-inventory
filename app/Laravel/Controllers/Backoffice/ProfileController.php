<?php

namespace App\Laravel\Controllers\Backoffice;

use App\Laravel\Models\ImageSlider;
use App\Laravel\Models\User;
use App\Laravel\Requests\Backoffice\ProfileRequest;
use App\Laravel\Requests\Backoffice\PasswordRequest;
use Illuminate\Support\Facades\Auth;
use ImageUploader;

class ProfileController extends Controller
{

	protected $data = array();

    public function index() {
        $this->data['user'] = Auth::user();
    	return view('backoffice.profile', $this->data);
    }

    public function update_profile(ProfileRequest $request) {
        $user = User::find(1);
        $user->fill($request->only('email', 'name'));
        $user->save();
        session()->flash('notification-status',"success");
        session()->flash('notification-msg', "Your profile has been updated.");
    	return redirect()->route('backoffice.profile.index');
    }

    public function update_password(PasswordRequest $request) {
    	$user = Auth::user();
        $user->fill($request->only('password'));
        $user->save();
        session()->flash('notification-status',"success");
        session()->flash('notification-msg', "Your password has been updated");
        return redirect()->route('backoffice.profile.index');
    }

}
