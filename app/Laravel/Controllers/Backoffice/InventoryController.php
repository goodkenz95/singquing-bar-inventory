<?php

namespace App\Laravel\Controllers\Backoffice;

use App\Laravel\Requests\Backoffice\ProductRequest;
use App\Laravel\Models\Supplier;
use App\Laravel\Models\Product;
use App\Laravel\Models\PhysicalCount;

use ImageUploader,PDF;

class InventoryController extends Controller
{

	protected $data = array();

    public function __construct(){
        $this->data['statuses'] = ['active' => "Active",'inactive' => "Inactive"];
        $this->data['suppliers'] = ['' => "Choose supplier"] + Supplier::where('status','active')->pluck('name','id')->toArray();
    }

    public function index() {
        $this->data['stocks'] = PhysicalCount::orderBy('qty','DESC')->get();
    	return view('backoffice.inventory.index', $this->data);
    }

    public function create() {
    	return view('backoffice.inventory.create', $this->data);
    }

    public function store(ProductRequest $request) {
    	$new_product = new Product;
        $new_product->fill($request->all());
        $new_product->save();

        session()->flash('notification-status', 'success');
        session()->flash('notification-msg', 'New product has been added.');
        return redirect()->route('backoffice.inventory.index');
    }

    public function edit($id = 0) {
        $product = Product::find($id);
        if($product) {
            $this->data['product'] = $product;
            return view('backoffice.inventory.edit', $this->data);
        } else {
            $this->_record_not_found();
        }
    }

    public function update(ProductRequest $request, $id = 0) {
    	$product = Product::find($id);
        if($product) {
            $product->fill($request->all());

            $product->save();

            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'Product has been updated.');
            return redirect()->route('backoffice.inventory.index');
        } else {
            $this->_record_not_found();
        }
    }

    public function pdf(){
        $this->data['stocks'] = PhysicalCount::orderBy('qty','DESC')->get();
        $pdf = PDF::loadView('pdf.inventory', $this->data);
        return $pdf->stream("inventory.pdf");
    }

    public function destroy($id = 0) {
    	$product = Product::find($id);
        if($product) {
            $product->delete();
            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'A record has been deleted.');
            return redirect()->route('backoffice.inventory.index');
        } else {
            $this->_record_not_found();
        }
    }

    public function trash() {
        $this->data['users'] = Product::onlyTrashed()->orderBy('deleted_at', "desc")->get();
        return view('backoffice.inventory.trash', $this->data);
    }

    public function restore($id = 0) {
        $user = Product::onlyTrashed()->find($id);
        if($user) {
            $user->restore();
            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'A record has been restored.');
            return redirect()->route('backoffice.inventory.index');
        } else {
            $this->_record_not_found();
        }
    }

    private function _record_not_found() {
        session()->flash('notification-status', 'error');
        session()->flash('notification-msg', 'Record not found.');
        return redirect()->route('backoffice.inventory.index');
    }
}
