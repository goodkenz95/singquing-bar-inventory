<?php

namespace App\Laravel\Controllers\Backoffice;

use App\Laravel\Requests\Backoffice\SupplierRequest;
use App\Laravel\Models\Supplier;
use ImageUploader;

class SupplierController extends Controller
{

	protected $data = array();

    public function __construct(){
        $this->data['statuses'] = ['active' => "Active",'inactive' => "Inactive"];
    }

    public function index() {
        $this->data['suppliers'] = Supplier::orderBy('updated_at', "desc")->get();
    	return view('backoffice.supplier.index', $this->data);
    }

    public function create() {
    	return view('backoffice.supplier.create', $this->data);
    }

    public function store(SupplierRequest $request) {
    	$new_supplier = new Supplier;
        $new_supplier->fill($request->all());
        $new_supplier->save();

        session()->flash('notification-status', 'success');
        session()->flash('notification-msg', 'New supplier has been added.');
        return redirect()->route('backoffice.supplier.index');
    }

    public function edit($id = 0) {
        $supplier = Supplier::find($id);
        if($supplier) {
            $this->data['supplier'] = $supplier;
            return view('backoffice.supplier.edit', $this->data);
        } else {
            $this->_record_not_found();
        }
    }

    public function update(SupplierRequest $request, $id = 0) {
    	$supplier = Supplier::find($id);
        if($supplier) {
            $supplier->fill($request->all());

            $supplier->save();

            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'Supplier has been updated.');
            return redirect()->route('backoffice.supplier.index');
        } else {
            $this->_record_not_found();
        }
    }

    public function destroy($id = 0) {
    	$supplier = Supplier::find($id);
        if($supplier) {
            $supplier->delete();
            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'A record has been deleted.');
            return redirect()->route('backoffice.supplier.index');
        } else {
            $this->_record_not_found();
        }
    }

    public function trash() {
        $this->data['users'] = Supplier::onlyTrashed()->orderBy('deleted_at', "desc")->get();
        return view('backoffice.supplier.trash', $this->data);
    }

    public function restore($id = 0) {
        $user = Supplier::onlyTrashed()->find($id);
        if($user) {
            $user->restore();
            session()->flash('notification-status', 'success');
            session()->flash('notification-msg', 'A record has been restored.');
            return redirect()->route('backoffice.supplier.index');
        } else {
            $this->_record_not_found();
        }
    }

    private function _record_not_found() {
        session()->flash('notification-status', 'error');
        session()->flash('notification-msg', 'Record not found.');
        return redirect()->route('backoffice.supplier.index');
    }
}
