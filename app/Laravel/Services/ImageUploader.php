<?php

namespace App\Laravel\Services;

use Carbon, Input, Image, File, URL;

class ImageUploader {

	/**
	 * Store an input file
	 *
	 * @param string $input_file_name
	 * @param string $directory
	 *
	 * @return array
	 *
	 */
	public static function store($input_file_name = 'file', $directory = "uploads") {

		$response = array();
		$directory = rtrim($directory, "/");

		if(Input::hasFile($input_file_name)) {

			$file = Input::file($input_file_name);
			$ext = $file->getClientOriginalExtension();
			
			$path_directory = $directory . "/" . Carbon::now()->format('Ymd');
			$resized_directory = $path_directory . "/resized";
			$thumb_directory = $path_directory . "/thumbnails";

			if (!File::exists($path_directory)){
				File::makeDirectory($path_directory, $mode = 0777, true, true);
			}

			if (!File::exists($resized_directory)){
				File::makeDirectory($resized_directory, $mode = 0777, true, true);
			}

			if (!File::exists($thumb_directory)){
				File::makeDirectory($thumb_directory, $mode = 0777, true, true);
			}

			$filename = Helper::create_filename($ext);

			$file->move($path_directory, $filename); 
			Image::make("{$path_directory}/{$filename}")->save("{$resized_directory}/{$filename}", 95);
			Image::make("{$path_directory}/{$filename}")->fit(50,50)->save("{$thumb_directory}/{$filename}", 95);

			$response = [ "path" => $path_directory, "directory" => URL::to($path_directory), "filename" => $filename ];
		}

		return $response;
	}

	/**
	 * Delete an image file
	 *
	 * @param string $directory
	 * @param string $filename
	 *
	 * @return void
	 *
	 */
	public static function destroy($directory, $filename) {

		$directory = rtrim($directory, "/");

		$path_directory = $directory . "/" . $filename;
		$resized_directory = $directory . "/resized/" . $filename;
		$thumb_directory = $directory .  "/thumbnails/" . $filename;

		if(File::exists($path_directory)) {
			File::delete($path_directory);
		}

		if(File::exists($resized_directory)) {
			File::delete($resized_directory);
		}

		if(File::exists($thumb_directory)) {
			File::delete($thumb_directory);
		}
	}

	

}