<?php

namespace App\Laravel\Services;

use Illuminate\Support\Facades\Route;

class Helper {


	/**
	 * Create a filename with the given extension
	 *
	 * @param string $ext
	 *
	 * @return string
	 *
	 */
	public static function create_filename($ext) {
		return str_random(20). date("mdYhs") . "." . $ext;
	}

	public static function amount($number){
		return number_format($number,2,".",",");
	}

	/**
	 * Returns authentication routes
	 *
	 * @return Route
	 *
	 */
	public static function auth_routes() {
		return Route::group(['as' => "auth.", 'namespace' => "Auth"], function() {
			Route::get('login/{redirect_uri?}', ['as' => "login", 'uses' => "LoginController@login"]);
			Route::post('login/{redirect_uri?}', ['as' => "authenticate", 'uses' => "LoginController@authenticate"]);
			Route::any('logout', ['as' => "logout", 'uses' => "LoginController@logout"]);
			Route::get('register', ['as' => "register", 'uses' => "RegisterController@register"]);
			Route::post('register', ['as' => "store", 'uses' => "RegisterController@store"]);
			Route::get('forgot-password', ['as' => "forgot_password", 'uses' => "ForgotPasswordController@showForgotPasswordView"]);
			Route::post('forgot-password', ['as' => "sendPasswordResetToken", 'uses' => "ForgotPasswordController@sendPasswordResetToken"]);
			Route::get('reset-password/{token?}', ['as' => "reset_password", 'uses' => "ResetPasswordController@showResetPasswordView"]);
			Route::post('reset-password/{token?}', ['as' => "processPasswordResetToken", 'uses' => "ResetPasswordController@processPasswordResetToken"]);
		});
			
	}

	public static function date_format($time,$format = "M d, Y @ h:i a"){
		return $time == "0000-00-00 00:00:00" ? "" : date($format,strtotime($time));
	}

	/**
	 * Parse date to the standard sql date format
	 *
	 * @param date $time
	 * @param string $format
	 *
	 * @return Date
	 */
	public static function date_db($time) {
		return $time == "0000-00-00 00:00:00" ? "" : date(env('DATE_DB',"Y-m-d"),strtotime($time));
	}
}