@extends('backoffice._layouts.app')
@section('content')
<div class="robust-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="breadcrumb-wrapper col-xs-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('backoffice.index') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('backoffice.user.index') }}">Account Management</a></li>
          <li class="breadcrumb-item active">Add new Account</li>
        </ol>
      </div>
      <div class="content-header-left col-md-6 col-xs-12">
        <h3 class="content-header-title mb-0">Create new Account</h3>
        <p class="text-muted mb-0">Add a new account to your web application.</p>
      </div>
      <div class="content-header-right col-md-6 col-xs-12">
        <div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right mt-1">
          <a href="{{ route('backoffice.user.index') }}" class="btn btn-outline-primary"><i class="icon-plus"></i> All Records</a>
          {{-- <a href="{{ route('backoffice.user.trash') }}" class="btn btn-outline-info"><i class="icon-trash2"></i> Trash</a> --}}
        </div>
      </div>
      <div class="content-header-lead col-xs-12 mt-1">
        <p class="lead">
          {{-- Page Lead Paragraph --}}
        </p>
      </div>
    </div>
    <div class="content-body">
      <section id="horizontal-form-layouts">

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title" id="horz-layout-basic">Account Details</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
              </div>
              <div class="card-body collapse in">
                <div class="card-block">
                  <div class="card-text">
                   {{--  <p>This is where you enter the details of your page. Make sure to enter the data asked for each field. All fields marked with <code>*</code> are required.</p> --}}
                  </div>
                  <form class="form form-horizontal" method="post" enctype="multipart/form-data">
                    
                    <div class="form-body">
                      
                      {{ csrf_field() }}

                      <div class="form-group {{ $errors->has('name') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="name">Name of the Employee</label>
                        <div class="col-md-9">
                          <input type="text" id="name" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}">
                          @if($errors->has('name')) <small class="danger text-muted">{{ $errors->first('name') }}</small>@endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('username') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="username">Username</label>
                        <div class="col-md-9">
                          <input type="text" id="username" class="form-control" placeholder="eg. juandelacruz" name="username" value="{{ old('username') }}">
                           <small class="text-muted">If not provided, username will be based from the name.</small> @if($errors->has('username'))<small class="danger text-muted">{{ $errors->first('username') }}</small>@endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('contact') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="contact">Contact No.</label>
                        <div class="col-md-9">
                          <input type="text" id="contact" class="form-control" placeholder="eg. +63 900 1234567" name="contact" value="{{ old('contact') }}">
                          @if($errors->has('contact'))<small class="danger text-muted">{{ $errors->first('contact') }}</small>@endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('position') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="position">Job Position</label>
                        <div class="col-md-9">

                          {!!Form::select('position',$positions,old('position'),['class' => "form-control",'id' => "position"])!!}
                           @if($errors->has('position'))<small class="danger text-muted">{{ $errors->first('position') }}</small>@endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('gender') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="gender">Gender</label>
                        <div class="col-md-9">

                          {!!Form::select('gender',$genders,old('gender'),['class' => "form-control",'id' => "gender"])!!}
                           @if($errors->has('gender'))<small class="danger text-muted">{{ $errors->first('gender') }}</small>@endif
                        </div>
                      </div>

                      {{--<div class="form-group {{ $errors->has('type') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="type">Account Type</label>
                        <div class="col-md-9">

                          {!!Form::select('type',$types,old('type'),['class' => "form-control",'id' => "type"])!!}
                           @if($errors->has('type'))<small class="danger text-muted">{{ $errors->first('type') }}</small>@endif
                        </div>
                      </div>--}}

                      <div class="form-group {{ $errors->has('password') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="password">Account Password</label>
                        <div class="col-md-9">
                          <input type="password" id="password" class="form-control" placeholder="Account Password" name="password" value="">
                           @if($errors->has('password'))<small class="danger text-muted">{{ $errors->first('password') }}</small>@endif
                        </div>
                      </div>
                      <div class="form-group {{ $errors->has('password_confirmation') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="password_confirmation">Confirm Password</label>
                        <div class="col-md-9">
                          <input type="password" id="password_confirmation" class="form-control" placeholder="Confirm Password" name="password_confirmation" value="">
                        </div>
                      </div>

                    </div>

                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary mr-1">
                        <i class="icon-check2"></i> Add Account
                      </button>
                      <a href="{{ route('backoffice.page.index') }}" class="btn btn-default">
                        <i class="icon-cross2"></i> Cancel
                      </a>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- // Basic form layout section end -->
    </div>
  </div>
</div>
@stop

@section('vendor-css')
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/editors/codemirror.css">
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/editors/theme/monokai.css">
@stop

@section('page-styles')
@stop

@section('vendor-js')
<script src="/backoffice/robust-assets/js/plugins/editors/codemirror/lib/codemirror.js" type="text/javascript"></script>
<script src="/backoffice/robust-assets/js/plugins/editors/codemirror/mode/xml/xml.js" type="text/javascript"></script>
@stop

@section('page-scripts')
@stop