@extends('backoffice._layouts.app')

@section('preloader')
@include('backoffice._components.preloader')
@stop
@section('content')
<div class="robust-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="content-header-left col-md-6 col-xs-12">
        <h3 class="content-header-title mb-0">Dashboard and Statistics</h3>
        <p class="text-muted mb-0">Overview of the inventory</p>
      </div>
      <div class="content-header-lead col-xs-12 mt-1">
        {{-- <p class="lead">Dashboard Reporting will be placed here.</p> --}}
        <hr>
      </div>
    </div>
    <div class="content-body">
      <div class="row">
         <div class="col-xl-4 col-lg-4 col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-block">
                        <div class="media">
                            <div class="media-body text-xs-left">
                                <h3 class="success">{{number_format($total_stocks)}}</h3>
                                <span>Overall Total of Stocks</span>
                            </div>
                            <div class="media-right media-middle">
                                <i class="icon-cube success font-large-2 float-xs-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-12">
          <div class="card">
              <div class="card-body">
                  <div class="card-block">
                      <div class="media">
                          <div class="media-body text-xs-left">
                              <h3 class="deep-orange">{{number_format($stockin_today)}}</h3>
                              <span>Total Stock in today</span>
                          </div>
                          <div class="media-right media-middle">
                              <i class="icon-arrow-graph-up-right deep-orange font-large-2 float-xs-right"></i>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-block">
                    <div class="media">
                        <div class="media-body text-xs-left">
                            <h3 class="info">{{number_format($stockout_today)}}</h3>
                            <span>Total Stock out today</span>
                        </div>
                        <div class="media-right media-middle">
                            <i class="icon-arrow-graph-down-right info font-large-2 float-xs-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
      </div>
      <div class="row match-height">
        <div class="col-xl-6 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Stock-in this week</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    
                </div>
                <div class="card-body">
                    <div class="card-block">
                        <p>
                        {{-- Total posted {{number_format($thisweek_stockin_posted)}}, draft {{number_format($thisweek_stockin_draft)}}.  --}}
                        <span class="float-xs-right"><a href="{{route('backoffice.stock_in.index')}}" target="_blank">Stock-in Summary <i class="icon-arrow-right2"></i></a></span></p>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                                <tr>
                                    <th>Transaction Code</th>
                                    <th>Total QTY</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($thisweek_stockout as $index => $stock_out)
                                <tr>
                                    <td class="text-truncate"><a target="_blank" href="{{ route('backoffice.stock_out.pdf', [$stock_out->id]) }}">{{$stock_out->code}}</a></td>
                                    <td class="text-truncate center">{{$stock_out->total_qty}}</td>
                                    <td class="text-truncate">{{$stock_out->status}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-xl-6 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Stock-out this week</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    
                </div>
                <div class="card-body">
                    <div class="card-block">
                        <p>
                          {{-- Total posted {{number_format($thisweek_stockout_posted)}}, draft {{number_format($thisweek_stockout_draft)}}.  --}}

                        <span class="float-xs-right"><a href="{{route('backoffice.stock_out.index')}}" target="_blank">Stock-out Summary <i class="icon-arrow-right2"></i></a></span></p>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                                <tr>
                                    <th>Transaction Code</th>
                                    <th>Total QTY</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($thisweek_stockin as $index => $stock_in)
                                <tr>
                                    <td class="text-truncate"><a target="_blank" href="{{ route('backoffice.stock_in.pdf', [$stock_in->id]) }}">{{$stock_in->code}}</a></td>
                                    <td class="text-truncate center">{{$stock_in->total_qty}}</td>
                                    <td class="text-truncate">{{$stock_in->status}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>    
      </div>
    </div>
  </div>
</div>
@stop