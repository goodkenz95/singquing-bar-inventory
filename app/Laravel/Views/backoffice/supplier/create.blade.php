@extends('backoffice._layouts.app')
@section('content')
<div class="robust-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="breadcrumb-wrapper col-xs-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('backoffice.index') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('backoffice.supplier.index') }}">Suppliers</a></li>
          <li class="breadcrumb-item active">Add new Supplier</li>
        </ol>
      </div>
      <div class="content-header-left col-md-6 col-xs-12">
        <h3 class="content-header-title mb-0">Add new Supplier</h3>
        <p class="text-muted mb-0">Add a new supplier to your web application.</p>
      </div>
      <div class="content-header-right col-md-6 col-xs-12">
        <div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right mt-1">
          <a href="{{ route('backoffice.supplier.index') }}" class="btn btn-outline-primary"><i class="icon-plus"></i> All Records</a>
          {{-- <a href="{{ route('backoffice.supplier.trash') }}" class="btn btn-outline-info"><i class="icon-trash2"></i> Trash</a> --}}
        </div>
      </div>
      <div class="content-header-lead col-xs-12 mt-1">
        <p class="lead">
          {{-- Page Lead Paragraph --}}
        </p>
      </div>
    </div>
    <div class="content-body">
      <section id="horizontal-form-layouts">

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title" id="horz-layout-basic">Supplier Details</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
              </div>
              <div class="card-body collapse in">
                <div class="card-block">
                  <div class="card-text">
                   {{--  <p>This is where you enter the details of your page. Make sure to enter the data asked for each field. All fields marked with <code>*</code> are required.</p> --}}
                  </div>
                  <form class="form form-horizontal" method="post" enctype="multipart/form-data">
                    
                    <div class="form-body">
                      
                      {{ csrf_field() }}

                      <div class="form-group {{ $errors->has('name') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="name">Name of the Supplier</label>
                        <div class="col-md-9">
                          <input type="text" id="name" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}">
                          @if($errors->has('name')) <small class="danger text-muted">{{ $errors->first('name') }}</small>@endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('address') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="address">Address</label>
                        <div class="col-md-9">
                          <input type="text" id="address" class="form-control" placeholder="Supplier address if applicable" name="address" value="{{ old('address') }}">
                           @if($errors->has('address'))<small class="danger text-muted">{{ $errors->first('address') }}</small>@endif
                        </div>
                      </div>

                    </div>

                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary mr-1">
                        <i class="icon-check2"></i> Add Supplier
                      </button>
                      <a href="{{ route('backoffice.page.index') }}" class="btn btn-default">
                        <i class="icon-cross2"></i> Cancel
                      </a>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- // Basic form layout section end -->
    </div>
  </div>
</div>
@stop

@section('vendor-css')
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/editors/codemirror.css">
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/editors/theme/monokai.css">
@stop

@section('page-styles')
@stop

@section('vendor-js')
<script src="/backoffice/robust-assets/js/plugins/editors/codemirror/lib/codemirror.js" type="text/javascript"></script>
<script src="/backoffice/robust-assets/js/plugins/editors/codemirror/mode/xml/xml.js" type="text/javascript"></script>
@stop

@section('page-scripts')
@stop