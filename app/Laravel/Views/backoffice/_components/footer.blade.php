<footer class="footer footer-light navbar-fixed-bottom">
  <p class="clearfix text-muted text-sm-center mb-0 px-2">
	  <span class="float-md-left d-xs-block d-md-inline-block">
	  	Copyright  &copy; {{ Carbon\Carbon::now()->format("Y") }} 
	  	<a href="#" target="_blank" class="text-bold-800 grey darken-2">{{ config('app.name') }} </a>, All rights reserved.
	  </span>
  </p>
</footer>