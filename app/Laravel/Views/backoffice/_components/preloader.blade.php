<!-- START PRELOADER-->
<div id="preloader-wrapper">
  <div id="loader">
    <div class="line-scale-pulse-out-rapid loader-white">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  </div>
  <div class="loader-section section-top bg-primary"></div>
  <div class="loader-section section-bottom bg-primary"></div>
</div>
<!-- END PRELOADER-->