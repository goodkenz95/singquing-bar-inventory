<!-- main menu-->
<div id="main-menu" data-scroll-to-active="true" class="main-menu menu-dark menu-fixed menu-shadow menu-accordion">
  <!-- main menu header-->
  <div class="main-menu-header">
    
  </div>
  <!-- / main menu header-->
  <!-- main menu content-->
  <div class="main-menu-content">
    <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
      
      <li class="nav-item"><a href="{{ route('backoffice.index') }}"><i class="icon-home3"></i><span data-i18n="nav.dashboard" class="menu-title">Dashboard</span></a></li>
      @if(in_array(Str::lower(Auth::user()->position), ['administrator','owner','manager']))
      <li class="navigation-header"><span data-i18n="nav.content">MASTERFILE</span><i data-toggle="tooltip" data-placement="right" data-original-title="Tools" class="icon-ellipsis icon-ellipsis"></i></li>
      
      <li class="nav-item" data-group="product"><a href="#"><i class="icon-file-text2"></i><span data-i18n="nav.product" class="menu-title">Products</span></a>
        <ul class="menu-content">
          <li class=""><a href="{{ route('backoffice.product.index') }}" data-i18n="nav.product.all" class="menu-item">All Records</a></li>
          <li class=""><a href="{{ route('backoffice.product.create') }}" data-i18n="nav.product.create" class="menu-item">Add new Product</a></li>
        </ul>
      </li>

      <li class="nav-item" data-group="supplier"><a href="#"><i class="icon-file-text2"></i><span data-i18n="nav.route" class="menu-title">Suppliers</span></a>
        <ul class="menu-content">
          <li class=""><a href="{{ route('backoffice.supplier.index') }}" data-i18n="nav.supplier.all" class="menu-item">All Records</a></li>
          <li class=""><a href="{{ route('backoffice.supplier.create') }}" data-i18n="nav.supplier.create" class="menu-item">Add new Supplier</a></li>
        </ul>
      </li>
      @endif


      <li class="navigation-header"><span data-i18n="nav.content">TRANSACTIONS</span><i data-toggle="tooltip" data-placement="right" data-original-title="Tools" class="icon-ellipsis icon-ellipsis"></i></li>

      <li class="nav-item" data-group="inventory"><a href="{{ route('backoffice.inventory.index') }}"><i class="icon-home3"></i><span data-i18n="nav.inventory" class="menu-title">Physical Inventory</span></a></li>

      <li class="nav-item" data-group="stock-in"><a href="#"><i class="icon-images2"></i><span data-i18n="nav.stock_in" class="menu-title">Stock In</span></a>
        <ul class="menu-content">
          <li class=""><a href="{{ route('backoffice.stock_in.index') }}" data-i18n="nav.stock_in.all" class="menu-item">All Transactions</a></li>
          <li class=""><a href="{{ route('backoffice.stock_in.create') }}" data-i18n="nav.stock_in.create" class="menu-item">Add new Stock in</a></li>
        </ul>
      </li>

      <li class="nav-item" data-group="stock-out"><a href="#"><i class="icon-images2"></i><span data-i18n="nav.stock_in" class="menu-title">Stock Out</span></a>
        <ul class="menu-content">
          <li class=""><a href="{{ route('backoffice.stock_out.index') }}" data-i18n="nav.stock_out.all" class="menu-item">All Transactions</a></li>
          <li class=""><a href="{{ route('backoffice.stock_out.create') }}" data-i18n="nav.stock_out.create" class="menu-item">Add new Stock out</a></li>
        </ul>
      </li>

      @if(in_array(Str::lower(Auth::user()->position), ['administrator','owner']))
      <li class="navigation-header"><span data-i18n="nav.content">SYSTEM SETTINGS</span><i data-toggle="tooltip" data-placement="right" data-original-title="Tools" class="icon-ellipsis icon-ellipsis"></i></li>
      
      <li class="nav-item" data-group="user"><a href="#"><i class="icon-users"></i><span data-i18n="nav.page" class="menu-title">Account Management</span></a>
        <ul class="menu-content">
          <li class=""><a href="{{ route('backoffice.user.index') }}" data-i18n="nav.user.all" class="menu-item">All Accounts</a></li>
          <li class=""><a href="{{ route('backoffice.user.create') }}" data-i18n="nav.user.create" class="menu-item">Add New Account</a></li>
          {{-- <li class=""><a href="{{ route('backoffice.page.trash') }}" data-i18n="nav.page.trash" class="menu-item">Trashed Pages</a></li> --}}
        </ul>
      </li>
      @endif
      
      <li class="navigation-header"><span data-i18n="nav.content">Account Settings</span><i data-toggle="tooltip" data-placement="right" data-original-title="Tools" class="icon-ellipsis icon-ellipsis"></i></li>

      {{-- <li class="nav-item"><a href="{{ route('backoffice.profile.index') }}"><i class="icon-user"></i><span data-i18n="nav.profile" class="menu-title">Edit Profile</span></a></li> --}}

      <li class="nav-item"><a href="{{ route('backoffice.auth.logout') }}"><i class="icon-power3"></i><span data-i18n="nav.logout" class="menu-title">Logout</span></a></li>
    </ul>
  </div>
  <!-- /main menu content-->
</div>
<!-- / main menu-->