@extends('backoffice._layouts.app')
@section('content')
<div class="robust-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="breadcrumb-wrapper col-xs-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('backoffice.index') }}">Home</a></li>
          <li class="breadcrumb-item active">Stock In</li>
        </ol>
      </div>
      <div class="content-header-left col-md-6 col-xs-12">
        <h3 class="content-header-title mb-0">All Stock In</h3>
        <p class="text-muted mb-0">Record data of all stock-in of your products in your web application.</p>
      </div>
      <div class="content-header-right col-md-6 col-xs-12">
        <div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right mt-1">
          <a href="{{ route('backoffice.stock_in.create') }}" class="btn btn-outline-primary"><i class="icon-plus"></i> Add New Stock In</a>
          {{-- <a href="{{ route('backoffice.supplier.trash') }}" class="btn btn-outline-info"><i class="icon-trash2"></i> Trash</a> --}}
        </div>
      </div>
      </div>
      <div class="content-header-lead col-xs-12 mt-1">
        <p class="lead">
          {{-- Page Lead Paragraph --}}
        </p>
      </div>
    </div>
    <div class="content-body">
      

      <!-- Bootstrap 3 table -->
      <section id="bootstrap3">
       
        <div class="row">
          <div class="col-xs-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Record Data</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
              </div>
              <div class="card-body collapse in">
                <div class="card-block card-dashboard">
                  <p class="card-text">
                    {{-- DataTables can integrate seamlessly with Bootstrap 3 using Bootstrap's table styling options to present an interface with a uniform design, based on Bootstrap, for your site / app. --}}
                  </p>
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered bootstrap-3 datatable">
                      <thead>
                        <tr>
                          <th width="25%">Transaction Code</th>
                          <th width="10%">Total QTY</th>
                          <th>Status</th>
                          <th width="30%">People Involved</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($stock_ins as $index => $stock_in)
                        <tr>
                          <td>{{$stock_in->code}}</td>
                          <td>{{$stock_in->total_qty}}</td>
                          <td>{{$stock_in->status}}</td>
                          <td>
                           <div><small>Encoded By : <i><strong>{{$stock_in->encoder?$stock_in->encoder->name:"Anonymous"}}</strong></i></small></div>
                           @if($stock_in->status != "draft")
                           <div><small>{{$stock_in->status == "posted" ? "Approved" : "Cancelled"}} By : <i><strong>{{$stock_in->admin?$stock_in->admin->name:"Anonymous"}}</strong></i></small></div>
                           @endif
                          </td>
                          <td>
                            <div class="btn-group dropup">
                                <button type="button" class="btn btn-secondary btn-min-height dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{ route('backoffice.stock_in.edit', [$stock_in->id]) }}">View Details</a>
                                    <a class="dropdown-item" target="_blank" href="{{ route('backoffice.stock_in.pdf', [$stock_in->id]) }}">View PDF</a>
                                    @if($stock_in->status == "draft")
                                    <a class="dropdown-item btn-approve" data-url="{{ route('backoffice.stock_in.approve', [$stock_in->id]) }}" href="#">Approve and Post</a>
                                    <a class="dropdown-item btn-delete" data-url="{{ route('backoffice.stock_in.destroy', [$stock_in->id]) }}" href="#">Void Transaction</a>
                                    @endif
                                </div>
                            </div>
                          </td>

                          @endforeach
                        </tr>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>Transaction Code</th>
                          <th>Total QTY</th>
                          <th>Status</th>
                          <th>People Involved</th>
                          <th></th>
                        </tr>
                      </tfoot>
                    </table>  
                  </div>      
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--/ Bootstrap 3 table -->
    </div>
  </div>
</div>
@stop

@section('vendor-css')
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/extensions/sweetalert.css">
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/tables/datatable/dataTables.bootstrap4.min.css">
@stop

@section('page-styles')
@stop

@section('vendor-js')
<script src="/backoffice/robust-assets/js/plugins/extensions/sweetalert.min.js" type="text/javascript"></script>
<script src="/backoffice/robust-assets/js/plugins/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="/backoffice/robust-assets/js/plugins/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>

@stop

@section('page-scripts')
<script type="text/javascript">
  $(function(){

    $.extend( $.fn.dataTable.defaults, {
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    $('.datatable').DataTable({
        "columnDefs": [{
          "targets": [ -1 ],
          "orderable": false,
        }]
    });

   

    $('.datatable').delegate('.btn-delete','click', function(){
        var url = $(this).data('url');
        //Warning Message
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to undo this transaction!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, void it!",   
            closeOnConfirm: false 
        }, function(){   
            window.location.href = url;
        });
    }).delegate('.btn-approve','click', function(){
        var url = $(this).data('url');
        swal({   
             title: "Are you sure?",   
             text: "Approve transaction will be converted into actual inventory. ",   
             type: "warning",   
             showCancelButton: true,   
             confirmButtonColor: "#DD6B55",   
             confirmButtonText: "Approve and Post!",   
             closeOnConfirm: false 
        }, function(){   
             window.location.href = url;
        });
    });
  });
</script>



@stop