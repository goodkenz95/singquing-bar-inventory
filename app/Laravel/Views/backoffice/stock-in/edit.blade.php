@extends('backoffice._layouts.app')
@section('content')
<div class="robust-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="breadcrumb-wrapper col-xs-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('backoffice.index') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('backoffice.stock_in.index') }}">Stock In</a></li>
          <li class="breadcrumb-item active">Transaction Details : {{$header->code}}</li>
        </ol>
      </div>
      <div class="content-header-left col-md-6 col-xs-12">
        <h3 class="content-header-title mb-0">Transaction Details</h3>
        <p class="text-muted mb-0">Details of the stock in.</p>
      </div>
      <div class="content-header-right col-md-6 col-xs-12">
        <div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right mt-1">
          <a href="{{ route('backoffice.stock_in.index') }}" class="btn btn-outline-primary"><i class="icon-plus"></i> All Records</a>
          {{-- <a href="{{ route('backoffice.stock_in.create') }}" class="btn btn-outline-primary"><i class="icon-plus"></i> Add new Stock In</a> --}}
          {{-- <a href="{{ route('backoffice.stock_in.trash') }}" class="btn btn-outline-info"><i class="icon-trash2"></i> Trash</a> --}}
        </div>
      </div>
      <div class="content-header-lead col-xs-12 mt-1">
        <p class="lead">
          {{-- Page Lead Paragraph --}}
        </p>
      </div>
    </div>
    <div class="content-body">
      <section id="horizontal-form-layouts">

        <div class="row">
          <div class="col-md-4">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title" id="horz-layout-basic">Transaction Header</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
              </div>
              <div class="card-body collapse in">
                <div class="card-block">
                  <div class="card-text">
                   {{--  <p>This is where you enter the details of your page. Make sure to enter the data asked for each field. All fields marked with <code>*</code> are required.</p> --}}
                  </div>
                  <form class="form form-horizontal" method="post" enctype="multipart/form-data">
                    
                    <div class="form-body">
                      
                      {{ csrf_field() }}

                      

                      <div class="form-group row">
                        <label class="col-md-4 label-control" for="name">Code</label>
                        <div class="col-md-8">
                          <strong>{{$header->code}}</strong>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-md-4 label-control" for="name">Encoder</label>
                        <div class="col-md-8">
                          <strong>{{$header->encoder?$header->encoder->name:"Anonymous"}}</strong>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-md-4 label-control" for="name">No. of Qty.</label>
                        <div class="col-md-8">
                          <strong>{{$header->total_qty}}</strong>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-md-4 label-control" for="name">Status</label>
                        <div class="col-md-8">
                          <strong>{{$header->status}}</strong>
                        </div>
                      </div>

                    </div>

                    <div class="form-actions">
                      @if($header->status == "draft" AND in_array(Str::lower(Auth::user()->position), ['administrator','manager']))
                      <a class="btn-void btn btn-danger" data-url="{{ route('backoffice.stock_in.destroy', [$header->id]) }}" href="#"><i class="icon-ban"></i> Void</a>
                      <a class="btn-approve btn btn-primary" data-url="{{ route('backoffice.stock_in.approve', [$header->id]) }}" href="#"><i class="icon-check"></i> Approve</a>
                      @endif
                      <a href="{{ route('backoffice.stock_in.index') }}" class="btn btn-default">
                        <i class="icon-arrow-left"></i> Return to All Records
                      </a>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-8">
            @if($header->status == "draft")
            <div class="card">
              <div class="card-header">
                <h4 class="card-title" id="horz-layout-basic">Stock In Form</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
              </div>
              <div class="card-body collapse in">
                <div class="card-block">
                  <div class="card-text">
                    <form class="form form-horizontal" method="post" enctype="multipart/form-data" action="{{route('backoffice.stock_in.add_product',[$header->id])}}">
                      <div class="form-body">
                        {{ csrf_field() }}
                        <div class="col-md-6">
                          <div class="form-group">
                            <div class="col-md-12">
                              {!!Form::select('product_id',$products,old('product_id'),['class' => "form-control",'id' => "product_id"])!!}
                              @if($errors->has('product_id')) <small class="danger text-muted">{{ $errors->first('product_id') }}</small>@endif
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <div class="col-md-12">
                            <input type="text" id="qty" class="form-control" placeholder="No. of Qty" name="qty" value="{{ old('qty') }}">
                              @if($errors->has('qty')) <small class="danger text-muted">{{ $errors->first('qty') }}</small>@endif
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Add Product</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            @endif


            <div class="card">
              <div class="card-header">
                <h4 class="card-title" id="horz-layout-basic">Transaction Details</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
              </div>
              <div class="card-body collapse in">
                <div class="card-block">
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered bootstrap-3 datatable">
                        <thead>
                          <tr>
                            <th width="35%">Product</th>
                            <th width="25%">Supplier</th>
                            <th width="10%">QTY</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($details as $index => $detail)
                          <tr>
                            <td>{{$detail->product_name}}</td>
                            <td>{{$detail->supplier_name}}</td>
                            <td>{{$detail->qty}}</td>
                            <td>
                              <div class="btn-group dropup">
                                  <button type="button" class="btn btn-secondary btn-min-height dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>
                                  <div class="dropdown-menu">
                                      <a class="dropdown-item btn-delete" data-url="{{ route('backoffice.stock_in.remove_product', [$header->id,$detail->id]) }}" href="#">Remove Product</a>
                                  </div>
                              </div>
                            </td>

                            @endforeach
                          </tr>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>Product</th>
                            <th>Supplier</th>
                            <th>QTY</th>
                            <th></th>
                          </tr>
                        </tfoot>
                      </table>  
                    </div>      
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- // Basic form layout section end -->
    </div>
  </div>
</div>
@stop

@section('vendor-css')
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/extensions/sweetalert.css">
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/editors/codemirror.css">
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/editors/theme/monokai.css">
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/tables/datatable/dataTables.bootstrap4.min.css">
@stop

@section('page-styles')
@stop

@section('vendor-js')
<script src="/backoffice/robust-assets/js/plugins/extensions/sweetalert.min.js" type="text/javascript"></script>
<script src="/backoffice/robust-assets/js/plugins/editors/codemirror/lib/codemirror.js" type="text/javascript"></script>
<script src="/backoffice/robust-assets/js/plugins/editors/codemirror/mode/xml/xml.js" type="text/javascript"></script>
<script src="/backoffice/robust-assets/js/plugins/tables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="/backoffice/robust-assets/js/plugins/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
@stop

@section('page-scripts')
<script type="text/javascript">
  $(function(){

    $.extend( $.fn.dataTable.defaults, {
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    $('.datatable').DataTable({
        "columnDefs": [{
          "targets": [ -1 ],
          "orderable": false,
        }]
    });

    $(".btn-approve").on("click",function(){
        var url = $(this).data('url');
        swal({   
            title: "Are you sure?",   
            text: "Approve transaction will be converted into actual inventory. ",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Approve and Post!",   
            closeOnConfirm: false 
        }, function(){   
            window.location.href = url;
        });
    });

    $(".btn-void").on("click",function(){
        var url = $(this).data('url');
        //Warning Message
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to undo this record!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, void it!",   
            closeOnConfirm: false 
        }, function(){   
            window.location.href = url;
        });
    });

    $('.datatable').delegate('.btn-delete','click', function(){
        var url = $(this).data('url');
        //Warning Message
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to undo this record!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            closeOnConfirm: false 
        }, function(){   
            window.location.href = url;
        });
    });
  });
</script>
@stop