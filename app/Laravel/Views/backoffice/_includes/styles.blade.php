<!-- BEGIN VENDOR CSS-->
<link rel="stylesheet" href="/backoffice/robust-assets/css/vendors.min.css"/>
<!-- BEGIN VENDOR CSS-->
<!-- BEGIN Font icons-->
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/fonts/icomoon.css">
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/fonts/flag-icon-css/css/flag-icon.min.css">
<!-- END Font icons-->
<!-- BEGIN Plugins CSS-->
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/sliders/slick/slick.css">
<!-- END Plugins CSS-->

<!-- BEGIN Vendor CSS-->
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/extensions/toastr.css">
@yield('vendor-css')
<!-- END Vendor CSS-->
<!-- BEGIN ROBUST CSS-->
<link rel="stylesheet" href="/backoffice/robust-assets/css/app.min.css"/>
<!-- END ROBUST CSS-->
<!-- BEGIN Page Level CSS-->
<!-- END Page Level CSS-->
<!-- BEGIN Custom CSS-->
<link rel="stylesheet" type="text/css" href="/backoffice/assets/css/style.css">
<style type="text/css">
	.table .dropdown-menu {
        left:auto;
        right:0;
    }

    .table .dropdown-item {
        width: auto;
    }

    .table > tbody > tr > td {
        vertical-align: middle;
    }
    .dropup .dropdown-toggle::after {
	    font-family: icomoon;
	    content: "\ee9e";
	}

    .bg-primary,.btn-primary,.page-item.active .page-link, .page-item.active .page-link:focus, .page-item.active .page-link:hover,.btn-outline-primary:hover {
        background-color: #fc0100!important;
    }

    .btn-primary,.page-item.active .page-link, .page-item.active .page-link:focus, .page-item.active .page-link:hover,.btn-outline-primary {
        border-color: #fc0100!important;
    }

    .breadcrumb a,.btn-outline-primary{
        color:  #fc0100!important;
    }

    .btn-danger{
        background-color: #aaa!important;
        border-color: #a1a1a1!important;
    }

    .btn-danger:hover{
        background-color: #999!important;
        border-color: #919191!important;
    }

    .btn-default{
        color: #333;
    }
</style>
<!-- END Custom CSS-->
