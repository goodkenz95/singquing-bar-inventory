@extends('backoffice._layouts.app')
@section('content')
<div class="robust-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="breadcrumb-wrapper col-xs-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('backoffice.index') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('backoffice.page.index') }}">Pages</a></li>
          <li class="breadcrumb-item active">{{ $page->title }}</li>
        </ol>
      </div>
      <div class="content-header-left col-md-6 col-xs-12">
        <h3 class="content-header-title mb-0">{{ $page->title }}</h3>
        <p class="text-muted mb-0">Edit the details of this page.</p>
      </div>
      <div class="content-header-right col-md-6 col-xs-12">
        <div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right mt-1">
          <a href="#{{ route('backoffice.page.create') }}" class="btn btn-outline-primary"><i class="icon-plus"></i> Add New</a>
          <a href="{{ route('backoffice.page.trash') }}" class="btn btn-outline-info"><i class="icon-trash2"></i> Trash</a>
        </div>
      </div>
      <div class="content-header-lead col-xs-12 mt-1">
        <p class="lead">
          {{-- Page Lead Paragraph --}}
        </p>
      </div>
    </div>
    <div class="content-body">
      <section id="horizontal-form-layouts">

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title" id="horz-layout-basic">Page Details</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                    <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                    <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                    <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-body collapse in">
                <div class="card-block">
                  <div class="card-text">
                   {{--  <p>This is where you enter the details of your page. Make sure to enter the data asked for each field. All fields marked with <code>*</code> are required.</p> --}}
                  </div>
                  <form class="form form-horizontal" method="post" enctype="multipart/form-data">
                    
                    <div class="form-body">
                      
                      {{ csrf_field() }}

                      <div class="form-group {{ $errors->has('title') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="title">Title</label>
                        <div class="col-md-9">
                          <input type="text" id="title" class="form-control" placeholder="Title" name="title" value="{{ old('title', $page->title) }}">
                          @if($errors->has('title')) <small class="danger text-muted">{{ $errors->first('title') }}</small>@endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('slug') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="slug">Slug</label>
                        <div class="col-md-9">
                          <input type="text" id="slug" class="form-control" placeholder="Slug" name="slug" value="{{ old('slug', $page->slug) }}">
                          <small class="text-muted">If not provided, slug will be based from the title.</small> @if($errors->has('slug'))<small class="danger text-muted">{{ $errors->first('slug') }}</small>@endif
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-md-2 label-control">Stored Thumbnail</label>
                        <div class="col-md-9">
                          @if($page->directory AND $page->filename)
                          <img src="{{ "{$page->directory}/resized/{$page->filename}" }}" style="width: 300px; height: auto;" data-action="zoom" alt="Stored Thumbnail" class="rounded">
                          @else
                          <p class="text-muted"><i class="icon-blocked"></i> Thumbnail not available.</p>
                          @endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('file') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control">Thumbnail</label>
                        <div class="col-md-9">
                          <label id="projectinput8" class="file center-block">
                            <input type="file" id="file" name="file">
                            @if($errors->has('file')) <small class="danger text-muted">{{ $errors->first('file') }}</small>@endif
                            <span class="file-custom"></span>
                          </label>
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('excerpt') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="excerpt">Excerpt</label>
                        <div class="col-md-9">
                          <textarea id="excerpt" rows="5" class="form-control" name="excerpt" placeholder="Excerpt">{!! old('excerpt', $page->excerpt) !!}</textarea>
                          @if($errors->has('excerpt')) <small class="danger text-muted">{{ $errors->first('excerpt') }}</small>@endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('content') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="content">Content</label>
                        <div class="col-md-9">
                          <textarea id="content" rows="5" class="summernote" name="content" placeholder="Content">{!! old('content', $page->content) !!}</textarea>
                          @if($errors->has('content')) <small class="danger text-muted">{{ $errors->first('content') }}</small>@endif
                        </div>
                      </div>

                    </div>

                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary mr-1">
                        <i class="icon-check2"></i> Save
                      </button>
                      <a href="{{ route('backoffice.page.index') }}" class="btn btn-default">
                        <i class="icon-cross2"></i> Cancel
                      </a>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- // Basic form layout section end -->
    </div>
  </div>
</div>
@stop

@section('vendor-css')
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/editors/summernote.css">
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/editors/codemirror.css">
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/editors/theme/monokai.css">
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/extensions/zoom.css">
@stop

@section('page-styles')
@stop

@section('vendor-js')
<script src="/backoffice/robust-assets/js/plugins/editors/codemirror/lib/codemirror.js" type="text/javascript"></script>
<script src="/backoffice/robust-assets/js/plugins/editors/codemirror/mode/xml/xml.js" type="text/javascript"></script>
<script src="/backoffice/robust-assets/js/plugins/editors/summernote/summernote.js" type="text/javascript"></script>
<script src="/backoffice/robust-assets/js/plugins/extensions/transition.js" type="text/javascript"></script>
<script src="/backoffice/robust-assets/js/plugins/extensions/zoom.min.js" type="text/javascript"></script>
@stop

@section('page-scripts')
<script type="text/javascript">
  $(function(){
    $(".summernote").summernote({
        height: 450,
    });
  });
</script>
@stop