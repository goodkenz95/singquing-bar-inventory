@extends('backoffice._layouts.app')
@section('content')
<div class="robust-content content container-fluid">
  <div class="content-wrapper">
    <div class="content-header row">
      <div class="breadcrumb-wrapper col-xs-12">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('backoffice.index') }}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('backoffice.product.index') }}">Products</a></li>
          <li class="breadcrumb-item active">Add new Product</li>
        </ol>
      </div>
      <div class="content-header-left col-md-6 col-xs-12">
        <h3 class="content-header-title mb-0">Add new Product</h3>
        <p class="text-muted mb-0">Add a new product to your web application.</p>
      </div>
      <div class="content-header-right col-md-6 col-xs-12">
        <div role="group" aria-label="Button group with nested dropdown" class="btn-group float-md-right mt-1">
          <a href="{{ route('backoffice.product.index') }}" class="btn btn-outline-primary"><i class="icon-plus"></i> All Records</a>
          {{-- <a href="{{ route('backoffice.product.trash') }}" class="btn btn-outline-info"><i class="icon-trash2"></i> Trash</a> --}}
        </div>
      </div>
      <div class="content-header-lead col-xs-12 mt-1">
        <p class="lead">
          {{-- Page Lead Paragraph --}}
        </p>
      </div>
    </div>
    <div class="content-body">
      <section id="horizontal-form-layouts">

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title" id="horz-layout-basic">Product Details</h4>
                <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
              </div>
              <div class="card-body collapse in">
                <div class="card-block">
                  <div class="card-text">
                   {{--  <p>This is where you enter the details of your page. Make sure to enter the data asked for each field. All fields marked with <code>*</code> are required.</p> --}}
                  </div>
                  <form class="form form-horizontal" method="post" enctype="multipart/form-data">
                    
                    <div class="form-body">
                      
                      {{ csrf_field() }}

                      <div class="form-group {{ $errors->has('supplier_id') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="supplier_id">Supplier</label>
                        <div class="col-md-9">

                          {!!Form::select('supplier_id',$suppliers,old('supplier_id'),['class' => "form-control",'id' => "supplier_id"])!!}
                           @if($errors->has('supplier_id'))<small class="danger text-muted">{{ $errors->first('supplier_id') }}</small>@endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('name') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="name">Name of the Product</label>
                        <div class="col-md-9">
                          <input type="text" id="name" class="form-control" placeholder="Name" name="name" value="{{ old('name') }}">
                          @if($errors->has('name')) <small class="danger text-muted">{{ $errors->first('name') }}</small>@endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('price') ? "has-danger" : NULL }} row">
                        <label class="col-md-2 label-control" for="price">Product Cost</label>
                        <div class="col-md-9">
                          <input type="text" id="price" class="form-control" placeholder="Product price if applicable" name="price" value="{{ old('price') }}">
                           @if($errors->has('price'))<small class="danger text-muted">{{ $errors->first('price') }}</small>@endif
                        </div>
                      </div>

                    </div>

                    <div class="form-actions">
                      <button type="submit" class="btn btn-primary mr-1">
                        <i class="icon-check2"></i> Add Product
                      </button>
                      <a href="{{ route('backoffice.page.index') }}" class="btn btn-default">
                        <i class="icon-cross2"></i> Cancel
                      </a>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- // Basic form layout section end -->
    </div>
  </div>
</div>
@stop

@section('vendor-css')
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/editors/codemirror.css">
<link rel="stylesheet" type="text/css" href="/backoffice/robust-assets/css/plugins/editors/theme/monokai.css">
@stop

@section('page-styles')
@stop

@section('vendor-js')
<script src="/backoffice/robust-assets/js/plugins/editors/codemirror/lib/codemirror.js" type="text/javascript"></script>
<script src="/backoffice/robust-assets/js/plugins/editors/codemirror/mode/xml/xml.js" type="text/javascript"></script>
@stop

@section('page-scripts')
@stop