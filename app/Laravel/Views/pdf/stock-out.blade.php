<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Stock Out No. {{$header->code}}</title>
	<style type="text/css">
		body{
			font-size: 12px;
		}

	</style>
</head>
<body>
	<table width="100%" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th>
				<div>
					Encoder : <strong>{{$header->encoder?$header->encoder->name:"Anonymous"}}</strong>
				</div>
				<div>
					@if($header->status != "draft")
					{{$header->status == "posted" ? "Approved" : "Cancelled"}} By : <strong>{{$header->admin?$header->admin->name:"Anonymous"}}</strong>
					@else
					<i>Transaction On-going</i>
					@endif
				</div>
				</th>
				<th width="30%" style="text-align: right; vertical-align: text-bottom; font-size: 24px; color: #dd0000; border: 1px solid #333; padding: 5px;">{{$header->code}}</th>
			</tr>
			<tr style=" padding: 0px; ">
				<th></th>
				<th width="30%" style="text-align: right; font-size: 10px; color: #333; border: 1px solid #333; padding: 5px;">({{Str::upper($header->status)}})</th>
			</tr>
			<tr style=" padding: 0px; ">
				<th>
				</th>
				<th width="30%" style="text-align: right; font-size: 10px; color: #333;  padding: 5px;">Initial Date : {{$header->created_at}}</th>
			</tr>
			<tr style=" padding: 0px; ">
				<th></th>
				<th width="30%" style="text-align: right; font-size: 10px; color: #333;  padding: 5px;">Last Modified : {{$header->updated_at}}</th>
			</tr>
		</thead>
	</table>

	<hr>
	<p>Tranasction Details</p>
	<table width="100%" cellpadding="0" cellspacing="0">
		<thead>
			<tr style="background: #e2e2e2;">
				<th style="border: 1px solid #333;padding: 5px 7px;">Product</th>
				<th style="border: 1px solid #333;padding: 5px 7px;">Supplier</th>
				<th width="10%" style="border: 1px solid #333;padding: 5px 7px; text-align: center;">QTY</th>
				<th width="10%" style="border: 1px solid #333;padding: 5px 7px; text-align: right;">Cost</th>
				<th width="10%" style="border: 1px solid #333;padding: 5px 7px; text-align: right;">Total Cost</th>
			</tr>
		</thead>
		<tbody>
			<?php $sum = 0;?>
			@foreach($details as $index => $detail)
			<tr>
			  <td style="border: 1px solid #333;padding: 10px 7px;">{{$detail->product_name}}</td>
			  <td style="border: 1px solid #333;padding: 10px 7px;">{{$detail->supplier_name}}</td>
			  <td style="border: 1px solid #333;padding: 10px 7px; text-align: center;">{{$detail->qty}}</td>
			  <td style="border: 1px solid #333;padding: 10px 7px; text-align: right;">{{Helper::amount($detail->product?$detail->product->price:"0.00")}}</td>
			  <td style="border: 1px solid #333;padding: 10px 7px; text-align: right;">{{Helper::amount($detail->product?$detail->product->price*$detail->qty:"0.00")}}</td>
			  <?php $sum +=($detail->product?($detail->product->price*$detail->qty):0);?>
			  @endforeach
			</tr>
		</tbody>
		<tfoot>
			<tr style="background: #e2e2e2;">
				<th style="border: 1px solid #333;padding: 5px 7px;">Product</th>
				<th style="border: 1px solid #333;padding: 5px 7px;">Supplier</th>
				<th style="border: 1px solid #333;padding: 5px 7px; text-align: center;">QTY</th>
				<th style="border: 1px solid #333;padding: 5px 7px; text-align: right;">Cost</th>
				<th width="10%" style="border: 1px solid #333;padding: 5px 7px; text-align: right;">Total Cost</th>
			</tr>
		</tfoot>
	</table>	
	<br>
	<div style="margin-top: 20px;">Summary of Transaction:</div>
	<p>Total QTY: <strong style="color: #dd0000;">{{$header->total_qty}} items</strong></p>
	<p>Overall Total : <strong style="color: #dd0000;">{{Helper::amount($sum)}}</strong></p>
	<br>
	<p style="text-align: center; margin-top: 150px;">---End of Record---</p>
</body>
</html>