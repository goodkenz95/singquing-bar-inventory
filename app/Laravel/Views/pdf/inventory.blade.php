<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Physical Inventory as of Today {{Helper::date_format(Carbon::now(),"H:i a")}}</title>
	<style type="text/css">
		body{
			font-size: 12px;
		}

	</style>
</head>
<body>
	<table width="100%" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th>
				<div>
					Generated by : <strong>{{Auth::user()->name}}</strong>
				</div>
				</th>
				<th width="30%" style="text-align: right; vertical-align: text-bottom; font-size: 24px; color: #dd0000; border: 1px solid #333; padding: 5px;">PHYSICAL INVENTORY</th>
			</tr>
			<tr style=" padding: 0px; ">
				<th></th>
				<th width="30%" style="text-align: right; font-size: 10px; color: #333; border: 1px solid #333; padding: 5px;">({{Str::upper("UPDATED")}})</th>
			</tr>
			<tr style=" padding: 0px; ">
				<th>
				</th>
				<th width="30%" style="text-align: right; font-size: 10px; color: #333;  padding: 5px;">Generation Date : {{Helper::date_format(Carbon::now(),"Y-m-d h:i:s")}}</th>
			</tr>
		</thead>
	</table>

	<hr>
	<p>Physical Inventory Details</p>
	<table width="100%" cellpadding="0" cellspacing="0">
		<thead>
			<tr style="background: #e2e2e2;">
				<th style="border: 1px solid #333;padding: 5px 7px;">Product</th>
				<th style="border: 1px solid #333;padding: 5px 7px;">Supplier</th>
				<th width="10%" style="border: 1px solid #333;padding: 5px 7px; text-align: center;">QTY</th>
				<th width="10%" style="border: 1px solid #333;padding: 5px 7px; text-align: right;">Cost</th>
				<th width="10%" style="border: 1px solid #333;padding: 5px 7px; text-align: right;">Total Cost</th>
			</tr>
		</thead>
		<tbody>
			<?php $sum = 0;?>
			<?php $sum_qty = 0;?>
			@foreach($stocks as $index => $detail)
			<tr>
			  <td style="border: 1px solid #333;padding: 10px 7px;">{{$detail->product_name}}</td>
			  <td style="border: 1px solid #333;padding: 10px 7px;">{{$detail->supplier_name}}</td>
			  <td style="border: 1px solid #333;padding: 10px 7px; text-align: center;">{{$detail->qty}}</td>
			  <td style="border: 1px solid #333;padding: 10px 7px; text-align: right;">{{Helper::amount($detail->product?$detail->product->price:"0.00")}}</td>
			  <td style="border: 1px solid #333;padding: 10px 7px; text-align: right;">{{Helper::amount($detail->product?$detail->product->price*$detail->qty:"0.00")}}</td>
			  <?php $sum +=($detail->product?($detail->product->price*$detail->qty):0); $sum_qty+=$detail->qty;?>
			  @endforeach
			</tr>
		</tbody>
		<tfoot>
			<tr style="background: #e2e2e2;">
				<th style="border: 1px solid #333;padding: 5px 7px;">Product</th>
				<th style="border: 1px solid #333;padding: 5px 7px;">Supplier</th>
				<th style="border: 1px solid #333;padding: 5px 7px; text-align: center;">QTY</th>
				<th style="border: 1px solid #333;padding: 5px 7px; text-align: right;">Cost</th>
				<th width="10%" style="border: 1px solid #333;padding: 5px 7px; text-align: right;">Total Cost</th>
			</tr>
		</tfoot>
	</table>	
	<br>
	<div style="margin-top: 20px;">Total of Inventory as of :  {{Helper::date_format(Carbon::now(),"Y-m-d h:i A")}}</div>
	<p>Total QTY: <strong style="color: #dd0000;">{{$sum_qty}} items</strong></p>
	<p>Overall Total : <strong style="color: #dd0000;">{{Helper::amount($sum)}}</strong></p>
	<br>
	<p style="text-align: center; margin-top: 150px;">---End of Record---</p>
</body>
</html>