<?php


/**
 *
 * ------------------------------------
 * Backoffice Routes
 * ------------------------------------
 *
 */

Route::group(

	array(
		'as' => "backoffice.",
		'prefix' => "",
		'namespace' => "Backoffice",
	),

	function() {

		Helper::auth_routes();

		$this->group(['middleware' => "backoffice.auth"], function(){

			$this->get('/', ['as' => "index", 'uses' => "DashboardController@index"]);

			$this->group(['as' => "page.", 'prefix' => "pages"], function() {
				$this->get('/', ['as' => "index", 'uses' => "PageController@index"]);
				$this->get('create', ['as' => "create", 'uses' => "PageController@create"]);
				$this->post('create', ['as' => "store", 'uses' => "PageController@store"]);
				$this->get('edit/{id?}', ['as' => "edit", 'uses' => "PageController@edit"]);
				$this->post('edit/{id?}', ['as' => "update", 'uses' => "PageController@update"]);
				$this->get('trash', ['as' => "trash", 'uses' => "PageController@trash"]);
				$this->any('restore/{id?}', ['as' => "restore", 'uses' => "PageController@restore"]);
				$this->any('delete/{id?}', ['as' => "destroy", 'uses' => "PageController@destroy"]);
			});

			$this->group(['as' => "imageslider.", 'prefix' => "image-slider"], function() {
				$this->get('/', ['as' => "index", 'uses' => "ImageSliderController@index"]);
				$this->get('create', ['as' => "create", 'uses' => "ImageSliderController@create"]);
				$this->post('create', ['as' => "store", 'uses' => "ImageSliderController@store"]);
				$this->get('edit/{id?}', ['as' => "edit", 'uses' => "ImageSliderController@edit"]);
				$this->post('edit/{id?}', ['as' => "update", 'uses' => "ImageSliderController@update"]);
				$this->get('trash', ['as' => "trash", 'uses' => "ImageSliderController@trash"]);
				$this->any('restore/{id?}', ['as' => "restore", 'uses' => "ImageSliderController@restore"]);
				$this->any('delete/{id?}', ['as' => "destroy", 'uses' => "ImageSliderController@destroy"]);
			});

			$this->group(['as' => "user.", 'prefix' => "user"], function() {
				$this->get('/', ['as' => "index", 'uses' => "UserController@index"]);
				$this->get('create', ['as' => "create", 'uses' => "UserController@create"]);
				$this->post('create', ['as' => "store", 'uses' => "UserController@store"]);
				$this->get('edit/{id?}', ['as' => "edit", 'uses' => "UserController@edit"]);
				$this->post('edit/{id?}', ['as' => "update", 'uses' => "UserController@update"]);
				$this->get('trash', ['as' => "trash", 'uses' => "UserController@trash"]);
				$this->any('restore/{id?}', ['as' => "restore", 'uses' => "UserController@restore"]);
				$this->any('delete/{id?}', ['as' => "destroy", 'uses' => "UserController@destroy"]);
			});


			$this->group(['as' => "supplier.", 'prefix' => "supplier"], function() {
				$this->get('/', ['as' => "index", 'uses' => "SupplierController@index"]);
				$this->get('create', ['as' => "create", 'uses' => "SupplierController@create"]);
				$this->post('create', ['as' => "store", 'uses' => "SupplierController@store"]);
				$this->get('edit/{id?}', ['as' => "edit", 'uses' => "SupplierController@edit"]);
				$this->post('edit/{id?}', ['as' => "update", 'uses' => "SupplierController@update"]);
				$this->get('trash', ['as' => "trash", 'uses' => "SupplierController@trash"]);
				$this->any('restore/{id?}', ['as' => "restore", 'uses' => "SupplierController@restore"]);
				$this->any('delete/{id?}', ['as' => "destroy", 'uses' => "SupplierController@destroy"]);
			});

			$this->group(['as' => "inventory.",'prefix' => "inventory"],function(){
				$this->get('/',['as' => "index",'uses' => "InventoryController@index"]);
				$this->get('stock.pdf', ['as' => "pdf", 'uses' => "InventoryController@pdf"]);

			});

			$this->group(['as' => "stock_in.", 'prefix' => "stock-in"], function() {
				$this->get('/', ['as' => "index", 'uses' => "StockInController@index"]);
				$this->get('create', ['as' => "create", 'uses' => "StockInController@create"]);
				$this->post('create', ['as' => "store", 'uses' => "StockInController@store"]);
				$this->get('detail/{id?}', ['as' => "edit", 'uses' => "StockInController@edit"]);
				$this->post('detail/{id?}', ['as' => "update", 'uses' => "StockInController@update"]);

				$this->get('{id?}.pdf', ['as' => "pdf", 'uses' => "StockInController@pdf"]);

				$this->get('trash', ['as' => "trash", 'uses' => "StockInController@trash"]);
				$this->any('restore/{id?}', ['as' => "restore", 'uses' => "StockInController@restore"]);
				$this->any('delete/{id?}', ['as' => "destroy", 'uses' => "StockInController@destroy"]);

				$this->any('approve/{id?}', ['as' => "approve", 'uses' => "StockInController@approve"]);

				$this->post('add-product/{id?}',['as' => "add_product",'uses' => "StockInController@add_product"]);
				$this->any('remove-product/{id?}/{detail_id?}',['as' => "remove_product",'uses' => "StockInController@remove_product"]);
			});

			$this->group(['as' => "stock_out.", 'prefix' => "stock-out"], function() {
				$this->get('/', ['as' => "index", 'uses' => "StockOutController@index"]);
				$this->get('create', ['as' => "create", 'uses' => "StockOutController@create"]);
				$this->post('create', ['as' => "store", 'uses' => "StockOutController@store"]);
				$this->get('detail/{id?}', ['as' => "edit", 'uses' => "StockOutController@edit"]);
				$this->post('detail/{id?}', ['as' => "update", 'uses' => "StockOutController@update"]);
				$this->get('{id?}.pdf', ['as' => "pdf", 'uses' => "StockOutController@pdf"]);
				
				$this->get('trash', ['as' => "trash", 'uses' => "StockOutController@trash"]);
				$this->any('restore/{id?}', ['as' => "restore", 'uses' => "StockOutController@restore"]);
				$this->any('delete/{id?}', ['as' => "destroy", 'uses' => "StockOutController@destroy"]);

				$this->any('approve/{id?}', ['as' => "approve", 'uses' => "StockOutController@approve"]);

				$this->post('add-product/{id?}',['as' => "add_product",'uses' => "StockOutController@add_product"]);
				$this->any('remove-product/{id?}/{detail_id?}',['as' => "remove_product",'uses' => "StockOutController@remove_product"]);
			});

			$this->group(['as' => "product.", 'prefix' => "product"], function() {
				$this->get('/', ['as' => "index", 'uses' => "ProductController@index"]);
				$this->get('create', ['as' => "create", 'uses' => "ProductController@create"]);
				$this->post('create', ['as' => "store", 'uses' => "ProductController@store"]);
				$this->get('edit/{id?}', ['as' => "edit", 'uses' => "ProductController@edit"]);
				$this->post('edit/{id?}', ['as' => "update", 'uses' => "ProductController@update"]);
				$this->get('trash', ['as' => "trash", 'uses' => "ProductController@trash"]);
				$this->any('restore/{id?}', ['as' => "restore", 'uses' => "ProductController@restore"]);
				$this->any('delete/{id?}', ['as' => "destroy", 'uses' => "ProductController@destroy"]);
			});

			$this->group(['as' => "profile.", 'prefix' => "profile"], function() {
				$this->get('/', ['as' => "index", 'uses' => "ProfileController@index"]);
				$this->group(['prefix' => "update"], function(){
					$this->post('profile', ['as' => "update_profile", 'uses' => "ProfileController@update_profile"]);
					$this->post('password', ['as' => "update_password", 'uses' => "ProfileController@update_password"]);
				});
			});
			
		});
	}
);